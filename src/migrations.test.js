const { Pool } = require('pg');
const fs = require('fs');

beforeEach(() => {
  jest.setTimeout(60000);
});

const compareRows = (data1, data2, logFilePrefix) => {
  const successLogs = logFilePrefix + '_success.log';
  const errorLogs = logFilePrefix + '_err.log';
  const success = [];
  const err = [];
  for (let i = 0; i < data1.length; i++) {
    const row1 = JSON.stringify(data1[i]);
    const row2 = JSON.stringify(data2[i]);
    if (row1 === row2) {
      success.push(row1);
    } else {
      err.push(row1 + ' <==> ' + row2);
    }
  }
  fs.writeFileSync(successLogs, success.join('\n'));
  fs.writeFileSync(errorLogs, err.join('\n'));
  expect(err).toEqual([]);
};

const readCsvFile = filename => fs.readFileSync(filename)
    .toString()
    .split('\n')
    .map(row => row.split(','));

const connectionPool = new Pool({
  max: 20,
  idleTimeoutMillis: 10000,
  connectionTimeoutMillis: 2000,
  host: 'localhost',
  user: process.env['DB_USER'],
  password: process.env['DB_PASSWORD'],
  database: process.env['DB_NAME'],
});

test('test first migration: add creator_id column to groups table', async () => {
  const client = await connectionPool.connect();
  const res = (await client.query('select id, creator_id from groups_1 order by id'))
    .rows
    .map(row => [row['id'], row['creator_id']]);
  console.log(res);
  const expextedData = readCsvFile('./assets/expected1.csv');
  console.log(expextedData);
  compareRows(res, expextedData, 'migration1');
  client.release();
});

test('test second migration: add id to queue_details', async () => {
  const client = await connectionPool.connect();
  const res = (await client.query('select queue_id, user_id, sequence_number from qd1'))
    .rows
    .map(row => [row['queue_id'], row['user_id'], row['sequence_number'] + '']);
  console.log(res);
  const expextedData = readCsvFile('./assets/expected2.csv');
  console.log(expextedData);
  compareRows(res, expextedData, 'migration2');
  client.release();
});

afterAll(async () => {
  await connectionPool.end();
});
