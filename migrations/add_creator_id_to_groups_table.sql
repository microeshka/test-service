-- MIGRATION1:

-- preparation
create table groups_1 as (select id, name, description, created_at, join_id from groups);
select * from groups_1;

-- migration code:
alter table groups_1 add column creator_id uuid;

update groups_1 set creator_id = d.user_id from (
    select g2u.group_id, g2u.user_id, g2u.role_id, g2r.name
    from groups2users as g2u left join groups2roles as g2r on g2r.id = g2u.role_id
    where g2r.name = 'OWNER'
) as d where groups_1.id = d.group_id;

alter table groups_1 alter column creator_id set not null;

select id, creator_id from groups_1 order by id;

-- cleanup
drop table groups_1;
