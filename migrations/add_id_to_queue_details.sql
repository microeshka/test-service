-- MIGRATION2:

-- preparation

insert into queues (id, title, description, creator_id, created_at, subject_id, start_date, end_date, closing_date) values
(uuid_generate_v4(), 'lab2', 'Lab1', 'd3447817-4c0a-44c4-af3d-d925d138c11a', now(), '969bdaba-d417-47cd-8ccd-05ef422455e9', now(), now(), now()),
(uuid_generate_v4(), 'lab2', 'Lab2', 'd3447817-4c0a-44c4-af3d-d925d138c11a', now(), '969bdaba-d417-47cd-8ccd-05ef422455e9', now(), now(), now());

create table qd1 (
    queue_id uuid,
    user_id uuid,
    sequence_number integer,
    turned_at timestamp
);

insert into qd1 values
('18fbf3f4-330e-4447-938c-28f25043f7a3', 'd3447817-4c0a-44c4-af3d-d925d138c11a', 1, now()),
('18fbf3f4-330e-4447-938c-28f25043f7a3', '580e18a4-5af2-4026-9079-3ea9e55c4a1b', 2, now()),
('18fbf3f4-330e-4447-938c-28f25043f7a3', 'c97a3639-e62d-49b0-945c-bbea432c1b12', 3, now()),
('18fbf3f4-330e-4447-938c-28f25043f7a3', '2b2d09c9-4c2d-4c3c-b119-731a65f2d364', 4, now());

-- migration code

alter table qd1 add column id uuid default uuid_generate_v4();
alter table qd1 add constraint pr primary key (id);

select queue_id, user_id, sequence_number, turned_at from qd1;
select id, queue_id, user_id, sequence_number, turned_at from qd1;

-- cleanup

drop table qd1;
